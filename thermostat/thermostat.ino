#include <Servo.h>
#include <LiquidCrystal.h>

// Set all the pin numbers
const int servoPin = 10;
const int thermometerPin = A0;
const int potentiometerPin = A1;
const int blPin = 7;
// Set the display
LiquidCrystal lcd(13, 12, 5, 4, 3, 2);
// Set all the servo-related stuff
Servo knobActuator;
const int angleLimits[2] = {0, 165};
int angle = angleLimits[0];

// Interval of updates in ms, and how many data points to keep in memory and average
const int interval = 100;
const int dataLength = 200;
int skips = 0;
const int skipsOnMove = 10;
//time back light is on for
int blOnLength = 0;
// 0: on motor change, 1: on target changed
int blLengths[2] = {50, 100};

// Battery level, currently only aesthetic as the arduino is powered by USB
int battLevel = 100;

// Set the temperature-related variables
const float tempLimits[2] = {18.0, 23.0};
float meanTemp = 0;
float targetTemp;
//not sure why here
float lastTargetTemp;
float roomTemps[dataLength];


// Obtain room temperature from the thermal sensor, the maths is stolen from the arduino starter kit
float getRoomTemp() {
    return ((analogRead(thermometerPin) * 125.0) / 256.0) - 50.0;
}

// Calculate the wanted target temperature from the potentiometer and temp limits
float getTargetTemp() {
    return ((tempLimits[1] - tempLimits[0])
            * analogRead(potentiometerPin) / 1023.0)
            + tempLimits[0];
}

// Add a new data point (and remove the last one)
void addMeasure(float newTemp) {
    for (int i = 1; i < dataLength; i++) {
        roomTemps[i - 1] = roomTemps[i];
    }
    roomTemps[dataLength - 1] = newTemp;
}

// Get the mean of the data points
float getMean() {
    float total = 0;
    for (int i = 0; i < dataLength; i++) {
        total += roomTemps[i];
    }
    return total / dataLength;
}

void setup() {
    // Set up the servo
    knobActuator.attach(servoPin);
    knobActuator.write(angle);
    // Setup the lcd
    lcd.begin(16, 2);
    pinMode(blPin, OUTPUT);
    // Record the first room temps
    for (int i = 0; i < dataLength; i++) {
        roomTemps[i] = getRoomTemp();
    }
}

// Update the LCD with the data
void updateLCD() {
    lcd.print("Temp:");
    lcd.print(meanTemp);
    lcd.print(" Batt:");
    lcd.setCursor(0, 1);
    lcd.print("Want:");
    lcd.print(targetTemp);
    lcd.print("  ");
    lcd.print(battLevel);
    lcd.setCursor(0, 0);    // Reset cursor to start
}


void loop() {
    // Get our temperatures (and move the old targetTemp to the last variable)
    addMeasure(getRoomTemp());
    lastTargetTemp = targetTemp;
    targetTemp = getTargetTemp();
    meanTemp = getMean();
    // If the target temp has changed significantly, put the BL on
    if (targetTemp > lastTargetTemp + 0.2 || targetTemp < lastTargetTemp - 0.2) {
        blOnLength = blLengths[1];
    }

    // If the BL is supposed to be on, turn it on and decrement the counter
    if (blOnLength != 0) {
        blOnLength--;
        digitalWrite(blPin, HIGH);
    // Turn it off if not
    } else {
        digitalWrite(blPin, LOW);
    }

    // If skips are not 0 (ie. the servo has recently changed), don't make any changes to the servo
    if (skips == 0) {
        // If the room temp is low, turn the heater to high
        if (meanTemp < targetTemp - 0.5) {
            angle = angleLimits[1];
        // If it's too high, turn the heater to low
        } else if (meanTemp > targetTemp + 0.5) {
            angle = angleLimits[0];
        // If it's in between, set the servo to half
        } else {
            angle = (angleLimits[0] + angleLimits[1]) / 2;
        }

        // Write the angle to the motor if it's different
        if (knobActuator.read() != angle) {
            knobActuator.write(angle);
            // Then wait 10 intervals to stop the temperature sensor's inaccuracy making the servo go nuts
            skips = skipsOnMove;
            // Turn the BL on
            blOnLength = blLengths[0];
        }
    // Otherwise, count the skips down one
    } else {
        skips--;
    }

    // Write our data to the LCD
    updateLCD();
    // Wait for (interval)ms until the next loop
    delay(interval);
}
