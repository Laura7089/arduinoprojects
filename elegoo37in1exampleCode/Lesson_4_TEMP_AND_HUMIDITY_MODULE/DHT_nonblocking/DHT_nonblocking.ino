//2018.6.28
#include <dht_nonblocking.h>

float temperature;
float humidity;

const int sensorPin = 8;
DHT_nonblocking dht_sensor(sensorPin, DHT_TYPE_11);

void setup() {
    Serial.begin(9600);
}

void loop() {
    if (dht_sensor.measure(&temperature, &humidity)) {
        Serial.print("T = ");
        Serial.print(temperature, 1);
        Serial.print(" deg. C, H = ");
        Serial.print(humidity, 1);
        Serial.println("%");
    }
    delay(10);
}
