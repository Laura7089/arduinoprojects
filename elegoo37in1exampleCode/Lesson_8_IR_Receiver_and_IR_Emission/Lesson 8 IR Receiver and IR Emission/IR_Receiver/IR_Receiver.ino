#include <IRremote.h>

const int ledPin = 13;
int state;
IRrecv irrecv(8);
decode_results results;

void setup() { 
    pinMode(LED, OUTPUT);
    Serial.begin(9600);
    irrecv.enableIRIn();
}

void loop() {
    irrecv.decode(&results);
    if (results.value == 1) {    
        state = HIGH;
    } else {
        state = LOW;
    }
    digitalWrite(ledPin, state);     
    Serial.println(results.value);
    irrecv.resume();
}
