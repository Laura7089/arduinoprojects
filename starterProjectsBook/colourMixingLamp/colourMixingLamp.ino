const int redLEDPin = 11;
const int greenLEDPin = 9;
const int blueLEDPin = 10;
int redValue = 0;
int greenValue = 0;
int blueValue = 0;

const int redSensorPin = A0;
const int greenSensorPin = A1;
const int blueSensorPin = A2;
int redSensorValue = 0;
int greenSensorValue = 0;
int blueSensorValue = 0;

void setup() {
    Serial.begin(9600);

    pinMode(redLEDPin, OUTPUT);
    pinMode(greenLEDPin, OUTPUT);
    pinMode(blueLEDPin, OUTPUT);
}

void loop() {
    redSensorValue = analogRead(redSensorPin);
    greenSensorValue = analogRead(greenSensorPin);
    blueSensorValue = analogRead(blueSensorPin);

    Serial.print("Raw sensor values \t Red: ");
    Serial.print(redSensorValue);
    Serial.print("\t Green: ");
    Serial.print(greenSensorValue);
    Serial.print("\t Blue: ");
    Serial.print(blueSensorValue);
    Serial.print("\n");
    
    redValue = redSensorValue / 4;
    greenValue = greenSensorValue / 4;
    blueValue = blueSensorValue / 4;

    analogWrite(redLEDPin, redValue);
    analogWrite(greenLEDPin, greenValue);
    analogWrite(blueLEDPin, blueValue);
}
