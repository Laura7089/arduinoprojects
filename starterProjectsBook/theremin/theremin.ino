int sensorHigh = 0;
int sensorLow = 1023;

int sensorValuesPast[3] = {0, 0, 0};
int sensorAvg = 0;

const int ledPin = 13;

void setup() {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, HIGH);

    int sensorValue;
    while (millis() < 5000) {
        sensorValue = analogRead(A0);
        if (sensorValue > sensorHigh) {
            sensorHigh = sensorValue;
        }
        if (sensorValue < sensorLow) {
            sensorLow = sensorValue;
        }
    }

    float midPoint = (sensorHigh + sensorLow) / 2;
    for (int i = 0; i < 3; i++) {
        sensorValuesPast[i] = midPoint;
    }
    digitalWrite(ledPin, LOW);
}

void loop() {
    sensorValuesPast[2] = sensorValuesPast[1];
    sensorValuesPast[1] = sensorValuesPast[0];
    sensorValuesPast[0] = analogRead(A0);
    sensorAvg = (sensorValuesPast[0] + sensorValuesPast[1] + sensorValuesPast[2]) / 3;

    int pitch = map(sensorAvg, sensorLow, sensorHigh, 50, 4000);
    tone(11, pitch, 20);
    delay(10);
}
