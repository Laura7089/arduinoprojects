// include <someLibrary>

const int switchPin = 10;
const int motorPin = 6;

int switchState = 0;

void setup() {
    pinMode(motorPin, OUTPUT);
    pinMode(switchPin, INPUT);
}

void loop() {
    digitalWrite(motorPin, digitalRead(switchPin));
}
