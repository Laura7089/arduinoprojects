const int sensorPin = A0;
const float baselineTemp = 20.0;
const float boundaries[3] = {22.0, 24.0, 26.0};

void setup() {
    Serial.begin(9600);

    for (int pinNum = 2; pinNum < 5; pinNum++) {
        pinMode(pinNum, OUTPUT);
        digitalWrite(pinNum, LOW);
    }
}

void loop() {
    int sensorVal = analogRead(sensorPin);
    Serial.print("Sensor value: ");
    Serial.print(sensorVal);
    float voltage = (sensorVal / 1024.0) * 5.0;
    Serial.print(", volts: " );
    Serial.print(voltage);
    float temperature = (voltage - 0.5) * 100.0;
    Serial.print(", degrees celsius: ");
    Serial.println(temperature);

    // if (temperature < baselineTemp) {
    //     digitalWrite(2, LOW);
    //     digitalWrite(3, LOW);
    //     digitalWrite(4, LOW);
    // } else if (temperature >= baselineTemp + 2 && temperature < baselineTemp + 4) {
    //     digitalWrite(2, HIGH);
    //     digitalWrite(3, LOW);
    //     digitalWrite(4, LOW);
    // } else if (temperature >= baselineTemp + 4 && temperature < baselineTemp + 6) {
    //     digitalWrite(2, HIGH);
    //     digitalWrite(3, HIGH);
    //     digitalWrite(4, LOW);
    // } else if (temperature >= baselineTemp + 6) {
    //     digitalWrite(2, HIGH);
    //     digitalWrite(3, HIGH);
    //     digitalWrite(4, HIGH);
    // }

    for (int i = 0; i < 3; i++) {
        if (temperature >= boundaries[i]) {
            digitalWrite(i + 2, HIGH);
        } else {
            digitalWrite(i + 2, LOW);
        }
    }
        
    delay(1);
}
