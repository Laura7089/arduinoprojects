#!/bin/bash

if [ $# -eq 0 ]; then
    printf "Usage:  ./newProject.sh [project name]"
else
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
    mkdir $1
    cp makefile.example $1/makefile
    cp project.ino.example $1/$1.ino
    chmod +w $1/makefile $1/$1.ino
fi
